#!/usr/bin/env ruby
#
# Data processing service to print stats per device_id for every interesting minute
#
# Author: Gaurav Gupta
# Date: 07/06/2022

# requires installing logging gem 
## gem install logging
require 'logging'

Logging.init :info, :success, :warn, :error, :debug
Logging.color_scheme(
  'standard',
  lines: {
    info: :white,
    success: :green,
    warn: :yellow,
    error: :red,
    debug: :blue
  }
)
Logging.appenders.stdout(color_scheme: 'standard')
$logger = Logging.logger['log']
$logger.add_appenders(Logging.appenders.stdout)
$logger.level = 0

# Convert input data strems to json format
def convert_to_json(file)
  if File.size?(file)
    input_file = File.open(file)
    file_data = input_file.read
    file_data = file_data.gsub(/^$\n/, '')
    final_output = []
    file_data.each_line do |line|
      line = line.strip
      @stream_hash = {}
      line.each_line do |l|
        l = l.split(%r{,\s*})
        l.each do |e|
          e = e.split(':')
          e[0] = e[0].delete(' ').to_sym
          e[1] = e[1].delete(' ').to_i
          @stream_hash[e[0]] = e[1]
        end
        final_output << @stream_hash
      end
    end
    return final_output unless final_output.empty?
  end
  raise 'Not a valid input file provided!'
end

# Sort data stream by all stream's timestamps
## @param [Array] Input data stream from node as array of hashes
## @return [Array] Input data stream from node as array of hashes
def sort_data_streams(node_data_stream)
  unless node_data_stream.empty?
    input_data_stream = node_data_stream.sort_by { |k| k[:timestamp] }
    $logger.info '[Step 1] Sorting Data streams by original timestamps...'
    # $logger.debug "Data streams sorted by timestamps:\n#{input_data_stream}\n"
    return input_data_stream unless input_data_stream.nil?
  end
  raise 'Empty input_data_stream received!'
end

# Method to round/floor epoch time down to nearest minute
## @param [integer] Epoch Unix timestamp
## @return [integer] Rounded epoch Unix timestamp
def floor_epoch_time(timestamp)
  rounded_epoch_time = timestamp.to_i / 60 * 60
  return rounded_epoch_time unless rounded_epoch_time.nil?
end

# Create an array of start timestamps for every interesting minute in data stream received
## This is a critical piece to accommodate the fact that servers can query at arbitray times
## @param [Array] Time sorted input data stream
## @return [Array] List of every interesting minute's start timestamp
def determine_start_timers(data_streams)
  minute_start_timers = []
  minute_start_timers << start_time = floor_epoch_time(data_streams[0][:timestamp])
  data_streams.map do |stream|
    minute_start_timers << start_time = floor_epoch_time(stream[:timestamp]) if (stream[:timestamp] - start_time).abs > 59
  end
  unless minute_start_timers.empty?
    $logger.info '[Step 2] Creating an array of start timestamps for every minute recorded in data stream received...'
    #$logger.debug "Epoch start timestamps of data streams received (in seconds):\n#{minute_start_timers}\n"
    return minute_start_timers
  end
  raise "Couldn't parse any start timers!"
end

# Group data streams by each minute to compose an array of hashes
## In other words, every element of array consists of bunch of records pertaining
### to each minute's time frame. Each record itself contains key/value pairs
## @param [Array] List of every interesting minute's start timestamp
## @param [Array] Input data stream from node as array of hashes
## @return [Array] List of data stremas grouped by each minute
def group_data_streams(start_timers, data_stream)
  grouped_streams = []
  start_timers.each do |timer|
    start_time = timer
    ## Assuming timestamp in data streams are in seconds since epoch, we use rounded
    ### epoch time as start time and end_time as 59 seconds ahead of start_time.
    ## Epoch minute interval changes at the 60th second from its start time. Example:
    ### 1611781600 = Wednesday, January 27, 2021 9:06:40 PM GMT
    ### 1611781659 = Wednesday, January 27, 2021 9:07:39 PM GMT
    ### 1611781660 = Wednesday, January 27, 2021 9:07:40 PM GMT
    end_time = start_time + 59
    per_minute_element = []
    data_stream.each do |stream|
      per_minute_element << stream if stream[:timestamp].between?(start_time, end_time)
    end
    grouped_streams.push(per_minute_element)
  end
  unless grouped_streams.empty?
    $logger.info '[Step 3] Creating an array grouped by each minute using epoch start/end timestamps...'
    #$logger.debug "Array of hashes where each element consists of all data streams per minute:\n" \
                  #"#{grouped_streams}\n"
    return grouped_streams
  end
  raise 'Creation of grouped_streams failed!'
end

# Print out the following for every device_id (sorted) for each minute (based on the timestamp field):
## device_id
## timestamp_start
### Round timestamp_start down to the minute's start time using 'floor_epoch_time' method
## minimum value
## maximum value
## average value
## @param [Array] List of every interesting minute's start timestamp
## @param [Array] Each minute's data stream array
def print_device_id_stats(device_ids, per_minute_stream)
  device_ids = device_ids.map(&:to_i).sort
  device_ids.each do |id|
    timestamp_start = floor_epoch_time(per_minute_stream[0][:timestamp])
    values = []
    per_minute_stream.each do |s|
      values << s[:value] if s[:device_id].to_i == id
    end
    min_value = values.min
    max_value = values.max
    # Assuming we have to calculate average value of all values for per device ID per minute
    ## and not the average of just min and max values
    avg_value = (values.inject(0, :+).to_f / values.length).round(1)
    # If the expectation was the latter
    ## avg_value = (min_value + max_value).to_f / 2
    # Could have used '$logger.info' for logging final output, however, refraining to avoid extra logging
    puts "device_id: #{id}, timestamp_start: #{timestamp_start}, min: #{min_value}, " \
          "max: #{max_value}, avg: #{avg_value}"
  end
end

# Wrapper method for printing min, max, avg values for every unique device id for each minute
## @params [Array] List of data stremas grouped by each minute
def print_all_details(grouped_streams)
  $logger.info '[Step 4] Final stats for every device_id for each minute with timestamp_start epoch time:'
  grouped_streams.each do |element|
    unique_ids = element.map { |k| k[:device_id] }.uniq
    print_device_id_stats(unique_ids, element)
  end
end

# Store command line arg in a variable
inputfile = ARGV[0]
# auto assign default input file name if undefined by user
inputfile ||= 'inputdata.txt'

input_data_stream = convert_to_json(inputfile)
data_stream = sort_data_streams(input_data_stream)
start_timers = determine_start_timers(data_stream)
grouped_streams = group_data_streams(start_timers, data_stream)
print_all_details(grouped_streams)
