# Requirememts

1. Ruby installed on the host machine. I had v2.6 installed, so to keep things simple, it is recommended to use the same version for running the script.
2. Following gems needs to be installed (escalate permissions if required to install gems):
- gem install require
- gem install logging

# Contents of the project directory

1. data_stream_service.rb -> Main script
2. inputdata.txt -> default data input file
3. bigfile.txt -> A large file consisting of 1000 input data streams to test service scalability. It is recommended to execute the script with 'time' command to determine how long script took to process 1000 rows.

# How to run the script

1. Change directory to gagupta4_take_home_assessment/ and make data_stream_service.rb executable via 'chmod +x data_stream_service.rb'.
2. Execute the script via ./data_stream_service.rb (make sure you are not changing directory post code pull).

NOTES:
1. There are two input files included in the project:

1.a. inputdata.txt -> Default input file that consists of limited number of inout streams to test output. Conditional assignment operator defaults to 'inputdata.txt' file as input if a custom filename is not passed as command line arg.

1.b. bigfile.txt -> Consists of 1000 rows created for scalability test. Consider executing teh script with 'time' utility to determine time taken by the script to process 1000 rows.

2. Feel free to create your own .txt files with data streams of your wish and run the script with filename as string cmd arg.
Example: time ./data_stream_service.rb 'another_file.txt'

2. Step by step output is muted to avoid crowding the console. If required, uncomment the following lines and rerun: 61, 87, 118, 119.

# Assumptions made

1. Timestamps are in seconds since Unix epoch.
2. Dashboard server is pulling stats from a node via RESTful API that outputs response in JSON, and so converting simple input data stream in json format.
3. It was not clear in the assessment draft whether average value is the average of all values of a device ID for each minute or its the avg of min and max values only. Assuming its the former - avg value of all values of a device ID per minute.

# Code logic flow

Taking a simple input data stream to walk through the code logic:
device_id: 1, value: 1, timestamp: 1611741600
device_id: 2, value: 1, timestamp: 1611741602
device_id: 1, value: 2, timestamp: 1611741601
device_id: 1, value: 6, timestamp: 1611741660
device_id: 2, value: 3, timestamp: 1611741659

1. Script starts with converting the input into json. So above inout gets converted into:
[{:device_id=>1, :value=>1, :timestamp=>1611741600}, {:device_id=>2, :value=>1, :timestamp=>1611741602}, {:device_id=>1, :value=>2, :timestamp=>1611741601}, {:device_id=>1, :value=>6, :timestamp=>1611741660}, {:device_id=>2, :value=>3, :timestamp=>1611741659}]

2. Next step is to sort above seen data streams (aka elements of array) by their original epoch timestamp. Above output gets sorted and new output is:
[{:device_id=>1, :value=>1, :timestamp=>1611741600}, {:device_id=>1, :value=>2, :timestamp=>1611741601}, {:device_id=>2, :value=>1, :timestamp=>1611741602}, {:device_id=>2, :value=>3, :timestamp=>1611741659}, {:device_id=>1, :value=>6, :timestamp=>1611741660}]

3. Once sorting is done, we create an array of start timestamps for every recorded minute. We accomplish this by:

3.a. Rounding off the timestamp of first data stream and setting it as the start time of first minute and it to the array.

3.b. End time is calculated by setting it as 59 seconds ahead of the start time as epoch time changes on the 60th second since start.

3.c. If the absolute difference of next stream's timestamp and start time is < 59, it is discarded, and if is greater, it is marked as start timer of the next interesting minute.

3.d. End timer is accordingly adjusted as 59 seconds ahead of the new start timer.

3.e. Repeat 3.c. and 3.d. for all records.

3.f. There are two advantage of this approach:
- Serves a critical/important fact that servers can query at arbitray times, so we need to look for each minute dynamically. 
- Makes per minute grouping of streams easier.

3.g. List of each minute's start timer:
[1611741600, 1611741660]

4. Next step is to create an array of hashes grouped by each minute using epoch start/end timestamps gathered above. Here, each element represent streams received for each distinct minute in ascending order of epoch time (sorted by timestamps earlier):
[[{:device_id=>1, :value=>1, :timestamp=>1611741600}, {:device_id=>1, :value=>2, :timestamp=>1611741601}, {:device_id=>2, :value=>1, :timestamp=>1611741602}, {:device_id=>2, :value=>3, :timestamp=>1611741659}], [{:device_id=>1, :value=>6, :timestamp=>1611741660}]]

5. Final step is to find out unique device IDs per element (per minute stream) in the above array and print out the min, max, avg value stats and timestamp_start for every device_id for each minute (each element):

device_id: 1, timestamp_start: 1611741600, min: 1, max: 2, avg: 1.5
device_id: 2, timestamp_start: 1611741600, min: 1, max: 3, avg: 2.0
device_id: 1, timestamp_start: 1611741660, min: 6, max: 6, avg: 6.0
